/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author arthur
 */
public class Stock {
    private Integer amount;
    private Integer minimum_amount;

    public Stock(Integer amount, Integer minimum_amount) {
        this.amount = amount;
        this.minimum_amount = minimum_amount;
    }

    public void remove_amount(Integer amount_removed) {
        this.amount = this.amount - amount_removed;
    }
    
    public void add_amount(Integer amount_added){
        this.amount = this.amount + amount_added;
    }

    public Integer getAmount() {
        return amount;
    }

    public Integer getMinimum_amount() {
        return minimum_amount;
    }

    public void setMinimum_amount(Integer minimum_amount) {
        this.minimum_amount = minimum_amount;
    }
    
    
}
