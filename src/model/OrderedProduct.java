/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;





/**
 *
 * @author arthur
 */
public class OrderedProduct {
    private Integer quantity;
    private Product productOrdeered;
    private Float totalValue;
    private String observation;

    public OrderedProduct() {
    }

    
    public OrderedProduct(Integer quantity, Product productOrdeered) {
        this.quantity = quantity;
        this.productOrdeered = productOrdeered;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Product getProductOrdeered() {
        return productOrdeered;
    }

    public void setProductOrdeered(Product productOrdeered) {
        this.productOrdeered = productOrdeered;
    }

    public Float getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(Float totalValue) {
        this.totalValue = totalValue;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }
    
    
    
}
