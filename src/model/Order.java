/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.Calendar;

/**
 *
 * @author arthur
 */
public class Order {
    private Calendar date;
    private ArrayList<OrderedProduct> orderedProducts;
    private Client orderOwner;
    private Payment pay;
    private Double totalValue;

    public Order(Client orderOwner) {
        this.orderOwner = orderOwner;
        this.pay = new Payment();
        this.date = Calendar.getInstance();
        this.totalValue = 0.0;
    }

    public Calendar getDate() {
        return date;
    }
    
    public void addOrderedProducts(OrderedProduct orderedProduct){
        this.orderedProducts = new ArrayList<>();
        this.orderedProducts.add(orderedProduct); 
    }

    public ArrayList<OrderedProduct> getOrderedProducts() {
        return orderedProducts;
    }

    public Client getOrderOwner() {
        return orderOwner;
    }

    public void payOrder(){
        pay.pay();
    }
    public Payment getPay() {
        return pay;
    }

    public void calculateTotalValue(){
        orderedProducts.stream().forEach((orderedProduct) -> {
            this.totalValue += getTotalValue();
        });
    }
    public Double getTotalValue() {
        return totalValue;
    }
    
    
}
