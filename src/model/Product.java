/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author arthur
 */
public class Product {
    private String name;
    private String price;
    private Stock stock;

    public Product(String name, String price,Integer stock_amount,
            Integer minimum_stock_amount) {
        this.name = name;
        this.price = price;
        stock = new Stock(stock_amount, minimum_stock_amount);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
    
    
}
