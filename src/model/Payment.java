/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author arthur
 */
public class Payment {
    boolean payment;

    public Payment() {
        this.payment = false;
    }

    
    public boolean isPay() {
        return payment;
    }

    public void pay() {
        this.payment = true;
    }

}
