/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import model.Employee;

/**
 *
 * @author arthur
 */
public class EmployeeControl {
    private Employee employee;

    public EmployeeControl(Employee employee) {
        this.employee = employee;
    }
    
    public EmployeeControl(String user,String password){
        this.employee = new Employee(user, password);
    }
    
    /*public void saveuser () throws IOException{
        FileWriter registro = new FileWriter("doc/registro.txt");
        PrintWriter writeFile = new PrintWriter(registro);
        
    }*/
    
    public boolean verefyLogin() throws FileNotFoundException, IOException{
        FileReader registro = new FileReader("doc/registro.txt");
        BufferedReader registroReader = new BufferedReader(registro);
        String user;
        String password;
        while(((user = registroReader.readLine()) != null) && 
                ((password = registroReader.readLine()) != null)){
            if(!user.equals(employee.getName())){
                return false;
            }
            if(!password.equals(employee.getPassword())){
                return false;
            }
        }
        return true;
    }
    
}
